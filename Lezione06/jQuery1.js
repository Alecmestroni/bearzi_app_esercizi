function cambiaColorePrimoLI(colore) {
    if (typeof colore === "object") {
        colore = colore.data;
    }
    //$("li.primo").css("color",colore);
    $("li.primo").css({
        color: colore,
        "background-color": "lightgrey",
        fontSize: "20px",
        //"font-size": "20px"
    });
    return false;
}

function addClassLG() {
    $("div#contenitore").addClass("lg");
    return false;
}

function removeClassLG() {
    $("div#contenitore").removeClass("lg");
    return false;
}

function toggleClassLG() {
    $("div#contenitore").toggleClass("lg");
    return false;
}

function spostaElemento() {
    var elemento = $("#ul1").find(".ultimo").detach();
    $("#ul2").append(elemento);
}

function riempiDescrizione() {
    $(".descrizione").html("Testo completo della descrizione");
}

function nascondiUl() {
    $("ul").hide(1000);
}

function visualizzaUl() {
    $("ul").show(1000);
}

$( document ).ready(function() {
    var pulsante2 = $("a#pulsante2");
    pulsante2.click(function () {
        cambiaColorePrimoLI("green");
        return false;
    });
    $("a#pulsante3").click(prova);
    $("a#pulsante4").bind("click","pink",cambiaColorePrimoLI);
    $("a#pulsante5").click("brown",cambiaColorePrimoLI);
    // aggiungo pulsante classe lg
    $("div#contenitore").append("<a id='pulsante6' href='#'>Aggiungi classe lg</a>");
    $("a#pulsante6").click(addClassLG);
    $("div#contenitore").append("<a id='pulsante7' href='#'>Togli classe lg</a>");
    $("a#pulsante7").click(removeClassLG);
    $("div#contenitore").append("<a id='pulsante8' href='#'>Toggle classe lg</a>");
    $("a#pulsante8").click(toggleClassLG);
    $("div#contenitore").append("<a id='pulsante9' href='#'>Sposta elemento</a>");
    $("a#pulsante9").click(spostaElemento);
    $("div#contenitore").append("<a id='pulsante10' href='#'>Riempi descrizione</a>");
    $("a#pulsante10").click(riempiDescrizione);
    $("div#contenitore").append("<a id='pulsante11' href='#'>Nascondi ul</a>");
    $("a#pulsante11").click(nascondiUl);
    $("div#contenitore").append("<a id='pulsante12' href='#'>Visualizza ul</a>");
    $("a#pulsante12").click(visualizzaUl);
});

function prova() {
    alert("prova");
    return false;
}

