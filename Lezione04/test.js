/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : test.js
 *******************************************/
/* stampo nome e cognome */
var nome; nome = "Fausto";
var cognome = "Sgobba";
var eta= 43;
console.log("ESERCIZIO JAVASCRIPT 1");
console.log(nome+" "+cognome+" "+ eta);
console.log ("eta tra 10 anni:"+ (eta+10));
console.log ("eta /2:"+(eta/2));
console.log ("eta %2:"+(eta%2));
console.log (eta);
console.log ("eta ++:"+(eta++)); /* ++ e -- hanno effetto sulla variabile nella riga dopo se messi dopo */
console.log (eta);
console.log ("eta --:"+(eta--));
console.log (eta);
console.log ("++ eta :"+(++eta)); /* ++ e -- hanno effetto sulla variabile nella stessa riga se messi prima */
console.log (eta);
console.log ("-- eta:"+(--eta));
console.log (eta); 
console.log ("eta x 5:" + (eta*=5));
console.log ("--------------------------------------");
console.log("true="+(true));
console.log("!true="+(!true));
console.log("false="+(false));
console.log("!false="+(!false));
console.log ("1=2:"+(1==2));
console.log("1!=2:"+(1!=2)); 
/* differenza tra == e ===; 
== (equality) determina se due espressioni hanno lo stesso valore, eventualmente applicando le opportune conversioni tra tipi per effettuare il confronto
=== (identity) determina se due espressioni hanno lo stesso valore ma senza effettuare conversioni (i due termini confrontati devono dunque essere dello stesso tipo)*/