/* ARRAY 
length: Sets or returns the number of elements in an array
pop() Removes the last element of an array, and returns that element
push() Adds new elements to the end of an array, and returns the new length
reverse() Reverses the order of the elements in an array
shift() Removes the first element of an array, and returns that element
splice() Adds/Removes elements from an array
unshift() Adds new elements to the beginning of an array, and returns the new length
join() Joins all elements of an array into a string
*/

/* STRING
split() Splits a string into an array of substrings
toUpperCase() Converts a string to uppercase letters
toLowerCase() Converts a string to lowercase letters
length: Returns the length of a string
charAt() Returns the character at the specified index (position)
indexOf() Returns the position of the first found occurrence of a specified value in a strin
lastIndexOf() Returns the position of the last found occurrence of a specified value in a string
replace() Searches a string for a specified value, or a regular expression, and returns a new string where the specified values are replaced
substr() Extracts the characters from a string, beginning at a specified start position, and through the specified number of character
*/

/* DATE
now() 
getDate() Returns the day of the month (from 1-31)
getDay() 	Returns the day of the week (from 0-6)
getFullYear() Returns the year
getHours() 	Returns the hour (from 0-23)
getMinutes() 	Returns the minutes (from 0-59)
getMonth() Returns the month (from 0-11)
getTime() Returns the number of milliseconds since midnight Jan 1 1970, and a specified date
getSeconds() Returns the seconds (from 0-59)
set ... 
*/

/* MATH
abs() Returns the absolute value of x
ceil() Returns x, rounded upwards to the nearest integer
floor() Returns x, rounded downwards to the nearest integer
max() Returns the number with the highest value
min() Returns the number with the lowest value
random() Returns a random number between 0 and 1
sqrt() Returns the square root of x
*/