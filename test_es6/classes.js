class Being {
    constructor() {
        console.log("new Being");
    }
    sayHi() {
        console.log("Hi, I am a being");
    }
}

class Animal extends Being {
    constructor(age) {
        super();
        this.age = age;
        console.log("new Animal");
        //var _this = this;
        //setTimeout(function() {_this.makeNoise()},1000);
        setTimeout(() => this.makeNoise(),1000);
        setTimeout(() => this.makeNoise(),3000);
    }
    sayHi() {
        console.log("Hi, I am an Animal and I am "+this.age);
    }
    makeNoise() {
    }
    run() {
        console.log("I am Running");
    }
    eat() {
        console.log("I am eating");
    }
}

class Cat extends Animal {
    constructor(age) {
        super(age);
        this.favourite_food = "Fish";
        this.noise = "Miao";
        console.log("new Cat");
    }
    sayHi() {
        console.log("Hi, I am a Cat and I am "+this.age);
    }
    makeNoise() {
        console.log(this.noise);
    }
    eat() {
        super.eat();
        console.log(this.favourite_food);
    }
}

class Dog extends Animal {
    constructor(age) {
        super(age);
        this.favourite_food = "Meat";
        this.noise = "Bau";
        console.log("new Dog");
        this.noiseInterval = setInterval(() => this.makeNoise(),500);
    }
    sayHi() {
        console.log("Hi, I am a Dog and I am "+this.age);
    }
    makeNoise() {
        console.log(this.noise);
    }
    shhht() {
        clearInterval(this.noiseInterval);
    }
    eat() {
        super.eat();
        console.log(this.favourite_food+" and I like it very much");
    }
}

var c = new Cat(3);
c.sayHi();
c.run();
c.eat();

console.log("---------");

var d = new Dog(2);
d.sayHi();
d.run();
d.eat();